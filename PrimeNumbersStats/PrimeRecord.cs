﻿using System.Collections.Generic;

namespace PrimeNumbersStats
{
    partial class Program
    {
        private class PrimeRecord
        {
            private int primeNumber;
            private readonly Dictionary<int, int> _followingPrimeCount;
            private double _encountered;

            public PrimeRecord(int primeNumber)
            {
                Encountered = 0;
                this.primeNumber = primeNumber;
                _followingPrimeCount = new Dictionary<int, int>()
                {
                    {1, 0},
                    {3, 0},
                    {7, 0},
                    {9, 0},
                };
            }

            private double Encountered
            {
                get => _encountered;
                set => _encountered = value;
            }

            public void Record(int followingPrime)
            {
                _followingPrimeCount[followingPrime]+=1;
                Encountered += 1;
            }

            public Dictionary<int, double> computeStatistics()
            {
                return new()
                {
                    {1, _followingPrimeCount[1] / Encountered},
                    {3, _followingPrimeCount[3] / Encountered},
                    {7, _followingPrimeCount[7] / Encountered},
                    {9, _followingPrimeCount[9] / Encountered},
                };
            }
        }
    }
}