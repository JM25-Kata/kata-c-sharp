﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PrimeNumbersStats
{
    partial class Program
    {
        static void Main(string[] args)
        {
            const string primeFilePath = "primes.txt";
            var previousDigit = 7; //Remove 2, 3, and 5. Also remove 7 but set it as first encountered number

            var records = new Dictionary<int, PrimeRecord>()
            {
                {1, new PrimeRecord(1)},
                {3, new PrimeRecord(3)},
                {7, new PrimeRecord(7)},
                {9, new PrimeRecord(9)},
            };

            using (var sr = new StreamReader(primeFilePath))
            {
                while (sr.Peek() >= 0)
                {
                    var lastDigit = NextNumber(sr);
                    records[previousDigit].Record(lastDigit);
                    previousDigit = lastDigit;
                }
            }

            PrintStats(records);
        }

        private static void PrintStats(Dictionary<int, PrimeRecord> records)
        {
            Console.WriteLine("From / To\t | 1\t|3\t|\t7|9");
            foreach (var (key, value) in records)
            {
                var stats = value.computeStatistics();
                Console.WriteLine(
                    $"{key} \t | {stats[1]:P2} \t | {stats[3]:P2} \t | {stats[7]:P2} \t | {stats[9]:P2} \t |");
            }
        }

        private static int NextNumber(StreamReader stram)
        {
            string line = stram.ReadLine();
            int lastDigit = Int32.Parse(line!.Last().ToString());
            return lastDigit;
        }
    }
}