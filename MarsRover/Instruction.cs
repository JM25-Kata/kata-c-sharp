﻿namespace MarsRover
{
    public enum Instruction
    {
        Move,
        Left,
        Right
    }
}