﻿namespace MarsRover
{
    public class RoverDeployer
    {
        public Rover Deploy(string roverCharacteristics)
        {
            InstructionReader instructionReader = new InstructionReader();
            var lines = roverCharacteristics.Split('\n');
            Rover rover = new Rover(lines[0]);
            rover.Instruct(instructionReader.Read(lines[1]));
            return rover;
        }
    }
}