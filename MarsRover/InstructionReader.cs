﻿using System.Collections.Generic;

namespace MarsRover
{
    public class InstructionReader
    {
        public ICollection<Instruction> Read(string instructionLine)
        {
            var list = new List<Instruction>();
            foreach (var letter in instructionLine)
            {
                switch (letter)
                {
                    case 'M':
                        list.Add(Instruction.Move);
                        break;
                    case 'L':
                        list.Add(Instruction.Left);
                        break;
                    case 'R':
                        list.Add(Instruction.Right);
                        break;
                }
            }

            return list;
        }
    }
}