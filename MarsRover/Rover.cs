﻿using System;
using System.Collections.Generic;

namespace MarsRover
{
    public class Rover
    {
        public Rover()
        {
            position = (0, 0);
            heading = Direction.North;
        }

        public (int x, int y) position;
        public Direction heading;

        public Rover(string values)
        {
            var valuesSplitted = values.Split(' ');
            position = (Int32.Parse(valuesSplitted[0]), Int32.Parse(valuesSplitted[1]));
            Heading = DirectionExtentions.FromChar(valuesSplitted[2]);
        }

        public (int x, int y) Position
        {
            get => position;
            set => position = value;
        }

        public Direction Heading
        {
            get => heading;
            set => heading = value;
        }

        public void MoveForward()
        {
            switch (Heading)
            {
                case Direction.West:
                    position.x--;
                    break;
                case Direction.North:
                    position.y++;
                    break;
                case Direction.South:
                    position.y--;
                    break;
                case Direction.East:
                    position.x++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void RotateLeft()
        {
            switch (Heading)
            {
                case Direction.North:
                    heading = Direction.West;
                    break;
                case Direction.West:
                    heading = Direction.South;
                    break;
                case Direction.South:
                    heading = Direction.East;
                    break;
                case Direction.East:
                    heading = Direction.North;
                    break;
            }
        }

        public void RotateRight()
        {
            switch (Heading)
            {
                case Direction.North:
                    heading = Direction.East;
                    break;
                case Direction.East:
                    heading = Direction.South;
                    break;
                case Direction.South:
                    heading = Direction.West;
                    break;
                case Direction.West:
                    heading = Direction.North;
                    break;
            }
        }

        public void Instruct(Instruction instruction)
        {
            switch (instruction)
            {
                case Instruction.Move:
                    MoveForward();
                    break;
                case Instruction.Left:
                    RotateLeft();
                    break;
                case Instruction.Right:
                    RotateRight();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(instruction), instruction, null);
            }
        }

        public void Instruct(IEnumerable<Instruction> instructionList)
        {
            foreach (var instruction in instructionList)
            {
                Instruct(instruction);
            }
        }
    }
}