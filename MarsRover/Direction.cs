﻿using System;

namespace MarsRover
{
    public enum Direction
    {
        West,
        North,
        South,
        East
    }

    public static class DirectionExtentions
    {
        public static Direction FromChar(string letter)
        {
            switch (letter)
            {
                case "E":
                    return Direction.East;
                case "N":
                    return Direction.North;
                case "S":
                    return Direction.South;
                case "W":
                    return Direction.West;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}