﻿namespace FruitBasket
{
    public class Promotion
    {
        public int _articleNumber;
        public int _discountedNumber;

        public int ArticleNumber
        {
            get => _articleNumber;
            set => _articleNumber = value;
        }

        public int DiscountedNumber
        {
            get => _discountedNumber;
            set => _discountedNumber = value;
        }
    }
}