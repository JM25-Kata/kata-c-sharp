﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace FruitBasket
{
    class Program
    {
        static void Main(string[] args)
        {
            var basket = Basket.Parse(new Dictionary<string, int>()
            {
                {"Apple", 4},
                {"Orange", 3},
                {"Watermelon", 5},
            });
            var priceTable = new Dictionary<string, double>()
            {
                {"Apple", .20},
                {"Orange", .50},
                {"Watermelon", 0.80},
            };
            var promoTable = new Dictionary<string, Promotion>()
            {
                {"Apple", new Promotion {ArticleNumber = 2, DiscountedNumber = 1}},
                {"Watermelon", new Promotion {ArticleNumber = 3, DiscountedNumber = 1}},
            };
            var caisse = new Caisse(priceTable, promoTable);
            Console.WriteLine($"Basket raw amount {caisse.BasketRawAmount(basket).ToString("C", CultureInfo.CurrentCulture)}");
            Console.WriteLine($"Basket discount amount {caisse.BasketDiscount(basket):C}");
            Console.WriteLine($"Basket net amount {caisse.NetAmoount(basket):C}");
        }
    }
}