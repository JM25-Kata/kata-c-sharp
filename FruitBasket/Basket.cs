﻿using System.Collections.Generic;

namespace FruitBasket
{
    public class Basket
    {
        private Dictionary<string, int> articleList;

        private Basket(Dictionary<string, int> dictionary)
        {
            articleList = new Dictionary<string, int>(dictionary);
        }

        public static Basket Parse(Dictionary<string, int> input)
        {
            var basket = new Basket(input);
            return basket;
        }

        public bool Contains(string item)
        {
            return articleList.ContainsKey(item);
        }

        public int TotalItem(string item)
        {
            return articleList[item];
        }

        public static Basket Parse(List<KeyValuePair<string, int>> input)
        {
            var articles = new Dictionary<string, int>();
            foreach (var (key, value) in input)
            {
                if (articles.ContainsKey(key))
                {
                    articles[key] += value;
                }
                else
                {
                    articles.Add(key, value);
                }
            }
            return new Basket(articles);
        }

        public bool? Empty()
        {
            return articleList.Count == 0;
        }

        public Dictionary<string, int> ArticleList()
        {
            return articleList;
        }
    }
}