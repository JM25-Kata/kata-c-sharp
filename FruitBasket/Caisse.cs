﻿using System.Collections.Generic;
using System.Linq;

namespace FruitBasket
{
    public class Caisse
    {
        private readonly Dictionary<string, double> _priceTable;
        private readonly Dictionary<string, Promotion> _promoTable;

        public Caisse(Dictionary<string, double> priceTable, Dictionary<string, Promotion> promoTable)
        {
            _priceTable = priceTable;
            _promoTable = promoTable;
        }

        public Caisse(Dictionary<string, double> priceTable)
        {
            _priceTable = priceTable;
            _promoTable = new Dictionary<string, Promotion>();
        }

        public double NetAmoount(Basket basket)
        {
            var rawAmount = BasketRawAmount(basket);
            var discountTotal = BasketDiscount(basket);

            var netAmount = rawAmount - discountTotal;
            return netAmount;
        }

        public double BasketRawAmount(Basket basket)
        {
            var rawAmount = basket.ArticleList().Aggregate(0.0, (sum, element) =>
            {
                _priceTable.TryGetValue(element.Key, out var priceTag);
                return sum + element.Value * priceTag;
            });
            return rawAmount;
        }

        public double BasketDiscount(Basket basket)
        {
            var discountTotal = basket.ArticleList().Aggregate(0.0, (sum, element) =>
            {
                if (_promoTable.TryGetValue(element.Key, out var promo))
                {
                    var discountedTimes = element.Value / promo._articleNumber;
                    var discountAmount = discountedTimes * _priceTable[element.Key];
                    return sum + discountAmount;
                }

                return sum;
            });

            return discountTotal;
        }
    }
}