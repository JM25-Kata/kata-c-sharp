﻿using MarsRover;
using NUnit.Framework;

namespace MarsRoverTests
{
    public class RoverDeployerTests
    {
        [Test]
        public void DeployRover()
        {
            //Initial (5;5;E) (MMLRLMRRRMMM)
            //(7;5;5), (7;5;N), (7;6;N), (7;6;O), (4;6;O)

            string roverCharacteristics = "5 5 E\nMMLRLMRRRMMM";

            var roverDeployer = new RoverDeployer();
            Rover rover = roverDeployer.Deploy(roverCharacteristics);
            Assert.AreEqual((4,6), rover.Position);
            Assert.AreEqual(Direction.West, rover.Heading);
        }
    }
}