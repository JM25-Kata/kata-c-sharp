﻿using System.Collections.Generic;
using System.Linq;
using MarsRover;
using NUnit.Framework;

namespace MarsRoverTests
{
    public class InstructionReaderTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ReadMove()
        {
            var instructionReader = new InstructionReader();
            string instructionLine = "M";
            var instructionList = instructionReader.Read(instructionLine);
            Assert.AreEqual(1, instructionList.Count);
            Assert.That(instructionList.Any(instruction => instruction == Instruction.Move));
        }
        
        [Test]
        public void ReadLeft()
        {
            var instructionReader = new InstructionReader();
            string instructionLine = "L";
            var instructionList = instructionReader.Read(instructionLine);
            Assert.AreEqual(1, instructionList.Count);
            Assert.That(instructionList.Any(instruction => instruction == Instruction.Left));
        }
        
        [Test]
        public void ReadRight()
        {
            var instructionReader = new InstructionReader();
            string instructionLine = "R";
            var instructionList = instructionReader.Read(instructionLine);
            Assert.AreEqual(1, instructionList.Count);
            Assert.That(instructionList.Any(instruction => instruction == Instruction.Right));
        }

        [Test]
        public void ReadMultipleInstruction()
        {
            var instructionLine = "MLRM";
            var instructionReader = new InstructionReader();
            var instructionList =instructionReader.Read(instructionLine);
            Assert.AreEqual(4, instructionList.Count);
            Assert.AreEqual(new List<Instruction> {
                Instruction.Move, Instruction.Left, Instruction.Right, Instruction.Move
            }, instructionList);
        }
    }
}