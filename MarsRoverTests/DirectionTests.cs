﻿using MarsRover;
using NUnit.Framework;

namespace MarsRoverTests
{
    public class DirectionTests
    {
        [Test]
        public void convertFromStringTest()
        {
            Assert.AreEqual(Direction.East, DirectionExtentions.FromChar("E"));
            Assert.AreEqual(Direction.North, DirectionExtentions.FromChar("N"));
            Assert.AreEqual(Direction.South, DirectionExtentions.FromChar("S"));
            Assert.AreEqual(Direction.West, DirectionExtentions.FromChar("W"));
        }
    }
}