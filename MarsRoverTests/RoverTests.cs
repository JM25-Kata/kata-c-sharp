using System.Collections.Generic;
using MarsRover;
using NUnit.Framework;

namespace MarsRoverTests
{
    public class RoverTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void MoveForward()
        {
            var rover = new Rover {Heading = Direction.North, position = (5, 5)};
            rover.MoveForward();
            Assert.AreEqual(rover.Position, (5,6));
            
            rover.Heading = Direction.East;
            rover.position = (5, 5);
            rover.MoveForward();
            Assert.AreEqual(rover.Position, (6,5));
            
            rover.Heading = Direction.West;
            rover.position = (5, 5);
            rover.MoveForward();
            Assert.AreEqual(rover.Position, (4,5));
            
            rover.Heading = Direction.South;
            rover.position = (5, 5);
            rover.MoveForward();
            Assert.AreEqual(rover.Position, (5,4));
        }

        [Test]
        public void RotateLeft()
        {
            var rover = new Rover();
            rover.RotateLeft();
            Assert.AreEqual(rover.position, (0,0));
            Assert.AreEqual(rover.Heading, Direction.West);
            rover.RotateLeft();
            Assert.AreEqual(rover.Heading, Direction.South);
            rover.RotateLeft();
            Assert.AreEqual(rover.Heading, Direction.East);
            rover.RotateLeft();
            Assert.AreEqual(rover.Heading, Direction.North);
        }
        
        [Test]
        public void RotateRight()
        {
            var rover = new Rover();
            rover.RotateRight();
            Assert.AreEqual(rover.position, (0,0));
            Assert.AreEqual(rover.Heading, Direction.East);
            rover.RotateRight();
            Assert.AreEqual(rover.Heading, Direction.South);
            rover.RotateRight();
            Assert.AreEqual(rover.Heading, Direction.West);
            rover.RotateRight();
            Assert.AreEqual(rover.Heading, Direction.North);
        }

        [Test]
        public void InstructMove()
        {
            var rover = new Rover {Heading = Direction.North, position = (4, 5)};
            rover.Instruct(Instruction.Move);
            Assert.AreEqual(rover.Position, (4,6));
            Assert.AreEqual(rover.Heading, Direction.North);
        }
        
        [Test]
        public void InstructLeft()
        {
            var rover = new Rover();
            rover.Instruct(Instruction.Left);
            Assert.AreEqual(rover.position, (0,0));
            Assert.AreEqual(rover.Heading, Direction.West);
            rover.Instruct(Instruction.Left);
            Assert.AreEqual(rover.Heading, Direction.South);
            rover.Instruct(Instruction.Left);
            Assert.AreEqual(rover.Heading, Direction.East);
            rover.Instruct(Instruction.Left);
            Assert.AreEqual(rover.Heading, Direction.North);
        }
        
        [Test]
        public void InstructRight()
        {
            var rover = new Rover();
            rover.Instruct(Instruction.Right);
            Assert.AreEqual(rover.position, (0,0));
            Assert.AreEqual(rover.Heading, Direction.East);
            rover.Instruct(Instruction.Right);
            Assert.AreEqual(rover.Heading, Direction.South);
            rover.Instruct(Instruction.Right);
            Assert.AreEqual(rover.Heading, Direction.West);
            rover.Instruct(Instruction.Right);
            Assert.AreEqual(rover.Heading, Direction.North);
        }

        [Test]
        public void InstructMultiple()
        {
            var rover = new Rover {Heading = Direction.South, position = (10, 32)};

            rover.Instruct(new List<Instruction> {
                Instruction.Move, Instruction.Left, Instruction.Right, Instruction.Move
            });
            
            Assert.AreEqual(Direction.South, rover.Heading);
            Assert.AreEqual((10,30), rover.position);
        }
    }
}