using System.Collections.Generic;
using FruitBasket;
using NUnit.Framework;

namespace FruitBasketTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ParseEmptyBasket()
        {
            var input = new Dictionary<string, int>();
            var basket = Basket.Parse(input);
            Assert.IsTrue(basket.Empty());
        }

        [Test]
        public void ParseOne()
        {
            var input = new Dictionary<string, int>()
            {
                { "Foo", 42 },
            };
            var basket = Basket.Parse(input);
            Assert.True(basket.Contains("Foo"));
            Assert.AreEqual(basket.TotalItem("Foo"), 42);
        }
        
        [Test]
        public void ParseMultiple()
        {
            var input = new Dictionary<string, int>()
            {
                { "Foo", 42 },
                { "Bar", 55},
            };
            var basket = Basket.Parse(input);
            Assert.True(basket.Contains("Foo"));
            Assert.True(basket.Contains("Bar"));
            Assert.AreEqual(basket.TotalItem("Foo"), 42);
            Assert.AreEqual(basket.TotalItem("Bar"), 55);
        }
        
        [Test]
        public void AgregateMultiple()
        {
            var input = new List<KeyValuePair<string, int>>()
            {
                new( "Foo", 42 ),
                new( "Bar", 55),
                new( "Foo", 3 ),
            };
            var basket = Basket.Parse(input);
            Assert.True(basket.Contains("Foo"));
            Assert.True(basket.Contains("Bar"));
            Assert.AreEqual(basket.TotalItem("Foo"), 45);
            Assert.AreEqual(basket.TotalItem("Bar"), 55);
        }

        [Test]
        public void RawAmountOneType()
        {
            var priceTable = new Dictionary<string, double>()
            {
                {"Foo", .2},
                {"Bar", .5},
            };
            
            var input = new List<KeyValuePair<string, int>>()
            {
                new( "Foo", 1 ),
            };
            var basket = Basket.Parse(input);
            var caisse = new Caisse(priceTable);
            Assert.AreEqual(caisse.NetAmoount(basket), 0.20);
            
            input = new List<KeyValuePair<string, int>>()
            {
                new( "Foo", 4 ),
            };
            basket = Basket.Parse(input);
            Assert.AreEqual(caisse.NetAmoount(basket), 0.80);
        }
        
        [Test]
        public void RawAmountDifferentType()
        {
            var input = new List<KeyValuePair<string, int>>()
            {
                new( "Foo", 1 ),
                new( "Bar", 2),
            };
            var priceTable = new Dictionary<string, double>()
            {
                {"Foo", .20},
                {"Bar", .50},
            };
            var basket = Basket.Parse(input);
            var caisse = new Caisse(priceTable);
            Assert.AreEqual(caisse.NetAmoount(basket), 1.20);
        }

        [Test]
        public void BuyOneGetOneFreePromo()
        {
            var input = new List<KeyValuePair<string, int>>()
            {
                new( "Foo", 2 ),
            };
            var priceTable = new Dictionary<string, double>()
            {
                {"Foo", .20},
            };
            var basket = Basket.Parse(input);
            var promoTable = new Dictionary<string, Promotion>()
            {
                {"Foo", new Promotion {ArticleNumber = 2, DiscountedNumber = 1}},
            };
            var caisse = new Caisse(priceTable, promoTable);
            Assert.AreEqual(caisse.NetAmoount(basket), .20);
            
            priceTable = new Dictionary<string, double>()
            {
                {"Foo", .40},
            };
            basket = Basket.Parse(input);
            caisse = new Caisse(priceTable, promoTable);
            Assert.AreEqual(caisse.NetAmoount(basket), .40);
        }
        
         [Test]
        public void OneDiscount()
        {
            var input = new List<KeyValuePair<string, int>>()
            {
                new( "Foo", 2 ),
                new("Bar", 10),
            };
            var priceTable = new Dictionary<string, double>()
            {
                {"Foo", .20},
                {"Bar", 1.0},
            };
            var basket = Basket.Parse(input);
            var promoTable = new Dictionary<string, Promotion>()
            {
                {"Foo", new Promotion {ArticleNumber = 2, DiscountedNumber = 1}},
            };
            var caisse = new Caisse(priceTable, promoTable);
            Assert.AreEqual(caisse.NetAmoount(basket), 10.20, 0.001);
        }
    }
}